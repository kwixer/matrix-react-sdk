/*
Copyright 2020 Albert Education

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


export const AlbertFeatureSet = {
    ACCOUNT_SETTINGS: "account_settings",
    ADVANCED: "advanced",
    LEFTPANEL_SETTINGS: "leftpanel_settings",
    ROOM_SETTINGS: "room_settings",
    ROOM_SETTINGS_ADVANCED: "room_settings_advanced"
}


const ALBERT_STUDENT_KEY = "kw_albert_student"

/**
 * Albert Education specific logic for teachers/students
 * If it's not the student, automatically assumed its the teacher
 */
class _Albert {
    constructor() {
        const storedValue = sessionStorage.getItem(ALBERT_STUDENT_KEY);
        this.isStudent = storedValue ? JSON.parse(storedValue) : true;
        console.log("Rod: sessionStorage: ", sessionStorage.getItem(ALBERT_STUDENT_KEY));
        console.log("Rod: is student: ", this.isStudent);
    }

    isFeatureActive(feature) {
        if (this.isStudent) {
            return false;
        }
        //teacher enable left panel + room settings
        //feature === AlbertFeatureSet.LEFTPANEL_SETTINGS
        else if(feature === AlbertFeatureSet.ROOM_SETTINGS) {
            return true;
        }
        return false;
    }

    setStudent(isStudent) {
        console.log("Rod: setting student", isStudent);
        this.isStudent = isStudent;
        sessionStorage.setItem(ALBERT_STUDENT_KEY, isStudent);
    }
}

if (global.kw_Albert === undefined) {
    global.kw_Albert = new _Albert();
}
export default global.kw_Albert;
export const Albert = global.kw_Albert;